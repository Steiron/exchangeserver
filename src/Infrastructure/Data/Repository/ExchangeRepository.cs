using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApplicationCore.Entities;
using ApplicationCore.Interfaces.Repositories;
using Microsoft.EntityFrameworkCore;
using Z.EntityFramework.Plus;

namespace Infrastructure.Data.Repository
{
    public class ExchangeRepository : IExchangeRepository
    {
        private ExchangeContext _exchangeContext;

        public ExchangeRepository(ExchangeContext exchangeContext)
        {
            _exchangeContext = exchangeContext;
        }

        public async Task<IReadOnlyCollection<Currency>> GetAllCurrency()
        {
            return await _exchangeContext.Currencies.IncludeFilter(c => c.DatePrices.OrderBy(d => d.Date).Last())
                .ToListAsync();
        }

        public async Task<Currency> GetByIdWithDate(int currencyId, int month, int year)
        {
            DateTime curDate = new DateTime(year, month, 1);

            Currency currency = await _exchangeContext.Currencies.FirstOrDefaultAsync(c => c.Id == currencyId);
            var entry = _exchangeContext.Entry(currency);
            await entry.Collection(c => c.DatePrices)
                .Query()
                .Where(d => d.Date < curDate)
                .OrderByDescending(d => d.Date)
                .Take(1)
                .LoadAsync();
            await entry.Collection(c => c.DatePrices)
                .Query()
                .Where(d => d.Date.Month == month && d.Date.Year == year)
                .OrderBy(d => d.Date)
                .LoadAsync();


            return currency;
        }
    }
}