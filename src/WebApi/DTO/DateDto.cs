namespace WebApi.DTO
{
    public class DateDto
    {
        public int Month { get; set; }
        public int Year { get; set; }
    }
}