using System.Threading.Tasks;
using ApplicationCore.DTO;
using ApplicationCore.Interfaces.Services;
using Microsoft.AspNetCore.Mvc;
using WebApi.DTO;

namespace WebApi.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class ExchangeController : ControllerBase
    {
        private IExchangeService _exchangeService;

        public ExchangeController(IExchangeService exchangeService)
        {
            _exchangeService = exchangeService;
        }

        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            var allCurrency = await _exchangeService.GetAllCurrency();
            return new ObjectResult(allCurrency);
        }

        [HttpPost("{id:int}", Name = "GetByIdWithDate")]
        public async Task<IActionResult> GetByIdWithDate(int id, [FromBody] DateDto dateDto)
        {
            CurrencyDto currencyDto = await _exchangeService.GetByIdWithDate(id, dateDto.Month, dateDto.Year);
            return currencyDto == null
                ? NotFound($"Не найдена валюта по id:{id}")
                : new ObjectResult(currencyDto);
        }
    }
}