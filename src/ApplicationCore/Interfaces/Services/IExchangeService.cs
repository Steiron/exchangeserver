using System.Collections.Generic;
using System.Threading.Tasks;
using ApplicationCore.DTO;

namespace ApplicationCore.Interfaces.Services
{
    public interface IExchangeService
    {
        Task<IReadOnlyCollection<CurrencyDto>> GetAllCurrency();
        Task<CurrencyDto> GetByIdWithDate(int currencyId, int month, int year);
    }
}