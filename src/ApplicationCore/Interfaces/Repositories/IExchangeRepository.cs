using System.Collections.Generic;
using System.Threading.Tasks;
using ApplicationCore.Entities;

namespace ApplicationCore.Interfaces.Repositories
{
    public interface IExchangeRepository
    {
        Task<IReadOnlyCollection<Currency>> GetAllCurrency();
        Task<Currency> GetByIdWithDate(int currencyId, int month, int year);
    }
}