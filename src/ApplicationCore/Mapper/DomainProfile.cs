using ApplicationCore.DTO;
using ApplicationCore.Entities;
using AutoMapper;

namespace ApplicationCore.Mapper
{
    public class DomainProfile : Profile
    {
        public DomainProfile()
        {
            CreateMap<Currency, CurrencyDto>()
                .ForMember(f => f.DatePriceDtos,
                    to => to.MapFrom(m => m.DatePrices));

            CreateMap<DatePrice, DatePriceDto>()
                .ForMember(f => f.CurrencyDto,
                    to => to.MapFrom(m => m.Currency))
                .ForMember(f => f.CurrencyDtoId,
                    to => to.MapFrom(m => m.CurrencyId));
        }
    }
}