using System;

namespace ApplicationCore.Entities
{
    public class DatePrice : BaseEntity
    {
        public DateTime Date { get; private set; }

        public decimal Price { get; private set; }

        public int CurrencyId { get; private set; }
        public Currency Currency { get; private set; }

        private DatePrice()
        {
        }

        public DatePrice(int currencyId, DateTime date, decimal price)
        {
            Date = date;
            Price = price;
            CurrencyId = currencyId;
        }
    }
}