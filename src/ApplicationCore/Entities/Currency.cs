using System.Collections.Generic;

namespace ApplicationCore.Entities
{
    public class Currency : BaseEntity
    {
        public string Name { get; private set; }
        public int Count { get; private set; }

        public ICollection<DatePrice> DatePrices { get; private set; }


        private Currency()
        {
        }

        public Currency(string name, int count)
        {
            Name = name;
            Count = count;
        }

       
    }
}