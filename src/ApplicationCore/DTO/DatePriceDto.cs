using System;
using Newtonsoft.Json;

namespace ApplicationCore.DTO
{
    public class DatePriceDto : BaseEntityDto
    {
        public DateTime Date { get; private set; }

        [JsonProperty("Rate")]
        public decimal Price { get; private set; }

        [JsonProperty("CurrencyId")]
        public int CurrencyDtoId { get; private set; }

        [JsonProperty("Currency")]
        public CurrencyDto CurrencyDto { get; private set; }

        private DatePriceDto()
        {
        }

        public DatePriceDto(int currencyDtoId, DateTime date, decimal price)
        {
            Date = date;
            Price = price;
            CurrencyDtoId = currencyDtoId;
        }

        
    }
}