using System.Collections.Generic;
using Newtonsoft.Json;

namespace ApplicationCore.DTO
{
    public class CurrencyDto : BaseEntityDto
    {
        public string Name { get; private set; }
        public int Count { get; private set; }

        [JsonProperty(PropertyName = "DatePrices")]
        public IReadOnlyCollection<DatePriceDto> DatePriceDtos { get; private set; }


        private CurrencyDto()
        {
        }

        public CurrencyDto(string name, int count)
        {
            Name = name;
            Count = count;
        }
    }
}