using System.Collections.Generic;
using System.Threading.Tasks;
using ApplicationCore.DTO;
using ApplicationCore.Entities;
using ApplicationCore.Interfaces.Repositories;
using ApplicationCore.Interfaces.Services;
using AutoMapper;

namespace ApplicationCore.Services
{
    public class ExchangeService : IExchangeService
    {
        private IExchangeRepository _repository;
        private IMapper _mapper;

        public ExchangeService(IExchangeRepository repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public async Task<IReadOnlyCollection<CurrencyDto>> GetAllCurrency()
        {
            IReadOnlyCollection<Currency> allCurrency = await _repository.GetAllCurrency();
            return _mapper.Map<IReadOnlyCollection<CurrencyDto>>(allCurrency);
        }

        public async Task<CurrencyDto> GetByIdWithDate(int currencyId, int month, int year)
        {
            Currency currency = await _repository.GetByIdWithDate(currencyId, month, year);
            return _mapper.Map<CurrencyDto>(currency);
        }
    }
}